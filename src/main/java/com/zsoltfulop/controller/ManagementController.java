package com.zsoltfulop.controller;

import com.zsoltfulop.service.ManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 */
@RequestMapping("/event")
@RestController
public class ManagementController {

    @Autowired
    ManagementService managementService;

    @RequestMapping(value = "/add/{updatedBy}", method = RequestMethod.POST)
    public void addEvent(@PathVariable @NotNull Integer updatedBy) {
        managementService.insertEvent(updatedBy);
    }

    @RequestMapping(value = "/update/{id}/updatedBy/{updatedBy}", method = RequestMethod.PUT)
    public void updateEvent(@PathVariable @NotNull Long id, @PathVariable @NotNull Integer updatedBy) {
        managementService.updateEvent(id, updatedBy);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteEvent(@PathVariable @NotNull Long id) {
        managementService.deleteEvent(id);
    }
}
