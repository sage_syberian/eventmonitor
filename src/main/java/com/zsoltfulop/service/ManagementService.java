package com.zsoltfulop.service;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 * <p>
 * Event management service to make changes on the database table.
 */
public interface ManagementService {

    /**
     * INSERTS an event into the database.
     *
     * @param updatedBy the name of the actor that performs the INSERT
     */
    void insertEvent(Integer updatedBy);

    /**
     * Changes updatedBy in the DB by the id of the event.
     *
     * @param id        the id used to find the event to be updated.
     * @param updatedBy the new updatedBy value.
     */
    void updateEvent(Long id, Integer updatedBy);

    /**
     * Changes the active event flag to false thus implying its deleted state.
     *
     * @param id the id of the event whose active flag is to be set to false.
     */
    void deleteEvent(Long id);
}
