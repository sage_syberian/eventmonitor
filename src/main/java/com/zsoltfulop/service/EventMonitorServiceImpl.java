package com.zsoltfulop.service;

import com.zsoltfulop.dto.EventDTO;
import com.zsoltfulop.dto.builder.EventDTOBuilder;
import com.zsoltfulop.entity.Event;
import com.zsoltfulop.entity.EventType;
import com.zsoltfulop.repository.EventMonitorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 * <p>
 * Spring integration class to manage the event monitoring flow.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class EventMonitorServiceImpl {

    private static Logger LOGGER = LoggerFactory.getLogger(EventMonitorServiceImpl.class);

    @Autowired
    private EventMonitorRepository eventMonitorRepository;

    @Autowired
    private SimpMessagingTemplate template;

    /**
     * Polls the database at the frequency defined in the value of the poll.frequency key in the
     * application.properties file. Only events with pushed FALSE are returned.
     *
     * @return returns all the events to be pushed to the websocket topic.
     */
    @InboundChannelAdapter(channel = "monitor", poller = @Poller(fixedRate = "${poll.frequency}"))
    public Iterable<Event> findEventsByPushedFalse() {
        Iterable<Event> events;

        try {
            events = eventMonitorRepository.findByPushed(Boolean.FALSE);
            LOGGER.info("Found events: {}", events);
            return events;
        } catch (Exception e) {
            LOGGER.error("Poll failed");
        }
        return null;
    }

    /**
     * Pushes the DB events to the websocket topic if their pushed flag is FALSE.
     *
     * @param events the events that were found by the findEventsByPushedFalse method.
     * @return returns TRUE if the push was successful and FALSE if is was not completed
     */
    @ServiceActivator(inputChannel = "monitor", outputChannel = "delete")
    public Boolean pushEvents(Iterable<Event> events) {
        List<EventDTO> eventDTOS = new ArrayList<>();

        try {
            events.forEach(event -> event.setPushed(Boolean.TRUE));
            events.forEach(event -> eventDTOS.add(EventDTOBuilder.build(event)));
            template.convertAndSend("/topic/entries", eventDTOS);
            eventMonitorRepository.save(events);
            LOGGER.info("Pushed events: {}", events);
            return Boolean.TRUE;
        } catch (Exception e) {
            LOGGER.error("Push failed.");
            return Boolean.FALSE;
        }
    }

    /**
     * Deletes the events with TRUE pushed flag and DELETED event type.
     */
    @ServiceActivator(inputChannel = "delete")
    public void removeProcessedEvents(Boolean processed) {
        if (processed.equals(Boolean.TRUE)) {
            try {
                Iterable<Event> events = eventMonitorRepository.removeByPushedAndEventType(
                        Boolean.TRUE, EventType.DELETE.name());
                LOGGER.info("Deleted events: {}", events);
            } catch (Exception e) {
                LOGGER.error("no such element");
            }
        } else {
            LOGGER.warn("Events will not be deleted until they are pushed successfully.");
        }
    }
}