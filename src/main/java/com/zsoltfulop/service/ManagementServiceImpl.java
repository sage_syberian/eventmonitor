package com.zsoltfulop.service;

import com.zsoltfulop.entity.Event;
import com.zsoltfulop.entity.EventType;
import com.zsoltfulop.repository.ManagementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class ManagementServiceImpl implements ManagementService {

    private static Logger LOGGER = LoggerFactory.getLogger(ManagementServiceImpl.class);

    @Autowired
    private ManagementRepository managementRepository;

    @Override
    public void insertEvent(Integer updatedBy) {
        LOGGER.info("To be inserted {}", updatedBy);
        Date date = new Date();
        Event event = new Event(EventType.INSERT.name(), date, updatedBy, Boolean.FALSE);

        try {
            managementRepository.save(event);
        } catch (Exception e) {
            LOGGER.error("Event could not be inserted: {}", e);
        }
    }

    @Override
    @Transactional
    public void updateEvent(Long id, Integer updatedBy) {
        Date date = new Date();

        try {
            Event event = managementRepository.findOne(id);
            if (null != event && !EventType.DELETE.name().equals(event.getEventType())) {
                event.setEventType(EventType.UPDATE.name());
                event.setUpdatedBy(updatedBy);
                event.setTimeStamp(date);
                event.setPushed(Boolean.FALSE);
                managementRepository.save(event);
                LOGGER.info("Event updated: {}", event);
            } else {
                LOGGER.warn("Only existing events can be updated.");
            }
        } catch (Exception e) {
            LOGGER.error("Event could not be updated: {}", e);
        }
    }

    @Override
    @Transactional
    public void deleteEvent(Long id) {
        Date date = new Date();

        try {
            Event event = managementRepository.findOne(id);
            if (null != event && !EventType.DELETE.name().equals(event.getEventType())) {
                LOGGER.info("Event to be deleted: {}", event);
                event.setPushed(Boolean.FALSE);
                event.setEventType(EventType.DELETE.name());
                event.setTimeStamp(date);
                managementRepository.save(event);
                LOGGER.info("Event deleted");
            } else {
                LOGGER.warn("Only existing events can be deleted.");
            }
        } catch (Exception e) {
            LOGGER.error("Event could not be deleted: {}", e);
        }
    }
}