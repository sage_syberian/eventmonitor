package com.zsoltfulop.dto.builder;

import com.zsoltfulop.dto.EventDTO;
import com.zsoltfulop.entity.Event;

/**
 * Created by fulopz on 05/12/2016.
 */
public class EventDTOBuilder {

    public static EventDTO build(Event event) {

        EventDTO eventDTO = new EventDTO(
                event.getEventType(),
                event.getId(),
                String.valueOf(event.getTimeStamp().getTime()).substring(0, 4),
                event.getUpdatedBy()
        );

        return eventDTO;
    }
}
