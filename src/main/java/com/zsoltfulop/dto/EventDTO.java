package com.zsoltfulop.dto;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 */
public class EventDTO {

    private String eventType;
    private Long id;
    private String timeStamp;
    private Integer updatedBy;


    public EventDTO(String eventType, Long id, String timeStamp, Integer updatedBy) {
        this.eventType = eventType;
        this.id = id;
        this.timeStamp = timeStamp;
        this.updatedBy = updatedBy;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "EventDTO{" +
                "eventType='" + eventType + '\'' +
                ", id='" + id + '\'' +
                ", timeStamp=" + timeStamp +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}
