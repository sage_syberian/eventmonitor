package com.zsoltfulop.repository;

import com.zsoltfulop.entity.Event;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 */
public interface ManagementRepository extends CrudRepository<Event, Long> {

}
