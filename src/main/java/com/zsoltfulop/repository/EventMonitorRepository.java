package com.zsoltfulop.repository;

import com.zsoltfulop.entity.Event;
import com.zsoltfulop.entity.EventType;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 */
public interface EventMonitorRepository extends CrudRepository<Event, Long> {
    Iterable<Event> findByPushed(Boolean pushed);

    Iterable<Event> removeByPushedAndEventType(Boolean pushed, String eventType);
}
