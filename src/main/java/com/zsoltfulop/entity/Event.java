package com.zsoltfulop.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ZSOLTFULOP on 03/12/2016.
 */
@Entity
@Table(name = "events")
public class Event {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String eventType;

    @Column
    private Date timeStamp;

    @Column
    private Integer updatedBy;

    @Column
    private Boolean pushed;

    public Event() {
    }

    public Event(String eventType, Date timeStamp, Integer updatedBy, Boolean pushed) {
        this.eventType = eventType;
        this.timeStamp = timeStamp;
        this.updatedBy = updatedBy;
        this.pushed = pushed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean getPushed() {
        return pushed;
    }

    public void setPushed(Boolean pushed) {
        this.pushed = pushed;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", eventType='" + eventType + '\'' +
                ", timeStamp=" + timeStamp +
                ", updatedBy='" + updatedBy + '\'' +
                ", pushed=" + pushed +
                '}';
    }
}
