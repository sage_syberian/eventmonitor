package com.zsoltfulop.entity;

/**
 * Created by silve on 06/12/2016.
 */
public enum EventType {
    INSERT, UPDATE, DELETE
}
