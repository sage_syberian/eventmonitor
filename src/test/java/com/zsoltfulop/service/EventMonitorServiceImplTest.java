package com.zsoltfulop.service;

import com.zsoltfulop.dto.EventDTO;
import com.zsoltfulop.entity.Event;
import com.zsoltfulop.entity.EventType;
import com.zsoltfulop.repository.EventMonitorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by fulopz on 06/12/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class EventMonitorServiceImplTest {

    @Mock
    Event event;

    @Mock
    private EventMonitorRepository eventMonitorRepository;

    @Mock
    private SimpMessagingTemplate template;

    @InjectMocks
    private EventMonitorServiceImpl eventMonitorService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findNewEventsSuccess() throws Exception {
        eventMonitorService.findEventsByPushedFalse();

        verify(eventMonitorRepository, Mockito.times(1)).findByPushed(Boolean.FALSE);
    }

    @Test
    public void findNewEventsException() throws Exception {
        doThrow(Exception.class).when(eventMonitorRepository).findByPushed(Boolean.FALSE);

        eventMonitorService.findEventsByPushedFalse();

        verify(eventMonitorRepository, times(1)).findByPushed(Boolean.FALSE);
    }

    @Test
    public void pushEventsSuccess() throws Exception {

        List<Event> events = new ArrayList<>();
        events.add(new Event(EventType.INSERT.name(), new Date(), 1, Boolean.FALSE));
        Iterable<Event> eventsIterable = events;

        eventMonitorService.pushEvents(events);

        verify(template, times(1)).convertAndSend(anyString(), anyListOf(EventDTO.class));
        verify(eventMonitorRepository, times(1)).save(events);
    }

    @Test
    public void pushEventsNull() throws Exception {

        Iterable<Event> events = null;

        eventMonitorService.pushEvents(events);

        verify(template, times(0)).convertAndSend(anyString(), anyListOf(EventDTO.class));
        verify(eventMonitorRepository, times(0)).save(events);
    }

    @Test
    public void removeProcessedEventsSuccess() throws Exception {
        eventMonitorService.removeProcessedEvents(Boolean.TRUE);

        verify(eventMonitorRepository, times(1))
                .removeByPushedAndEventType(Boolean.TRUE, EventType.DELETE.name());
    }

    @Test
    public void removeProcessedEventsException() throws Exception {
        doThrow(Exception.class).when(eventMonitorRepository).removeByPushedAndEventType(Boolean.TRUE,
                EventType.DELETE.name());

        eventMonitorService.removeProcessedEvents(Boolean.TRUE);
    }

    @Test
    public void removeProcessedEventsProcessingFailed() throws Exception {

        eventMonitorService.removeProcessedEvents(Boolean.FALSE);

        verify(eventMonitorRepository, times(0)).removeByPushedAndEventType(
                Boolean.TRUE, EventType.DELETE.name());

    }

}