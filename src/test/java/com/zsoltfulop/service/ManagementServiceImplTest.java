package com.zsoltfulop.service;

import com.zsoltfulop.entity.Event;
import com.zsoltfulop.entity.EventType;
import com.zsoltfulop.repository.ManagementRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

/**
 * Created by fulopz on 06/12/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ManagementServiceImplTest {

    @Mock
    private ManagementRepository managementRepository;

    @InjectMocks
    private ManagementServiceImpl managementService;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void insertEvent() throws Exception {
        managementService.insertEvent(1);

        verify(managementRepository, times(1)).save(Mockito.any(Event.class));
    }

    @Test
    public void insertEventWithException() throws Exception {
        doThrow(Exception.class).when(managementRepository).save(Mockito.any(Event.class));

        managementService.insertEvent(1);

        verify(managementRepository, times(1)).save(Mockito.any(Event.class));
    }

    @Test
    public void updateEvent() throws Exception {
        Date timeStamp = new Date();
        Event event = new Event(EventType.UPDATE.name(), timeStamp, 1, Boolean.TRUE);

        when(managementRepository.findOne(1L)).thenReturn(event);

        managementService.updateEvent(1l, 2);

        verify(managementRepository, times(1)).findOne(1L);
        assertEquals(event.getPushed(), Boolean.FALSE);
        assertEquals(event.getUpdatedBy(), Integer.valueOf(2));
        assertEquals(event.getEventType(), EventType.UPDATE.name());
        assertFalse(event.getTimeStamp() == timeStamp);

        verify(managementRepository, times(1)).save(event);
    }

    @Test
    public void updateEventNull() throws Exception {
        Event event = null;

        when(managementRepository.findOne(1L)).thenReturn(event);

        managementService.updateEvent(1l, 2);

        verify(managementRepository, times(0)).save(event);
    }

    @Test
    public void updateEventException() throws Exception {
        Date timeStamp = new Date();
        Event event = new Event(EventType.UPDATE.name(), timeStamp, 1, Boolean.TRUE);

        when(managementRepository.findOne(1L)).thenReturn(event);
        doThrow(Exception.class).when(managementRepository).save(event);

        managementService.updateEvent(1l, 2);

        verify(managementRepository, times(1)).save(event);
    }

    @Test
    public void deleteEvent() throws Exception {
        Date timeStamp = new Date();
        Event event = new Event(EventType.UPDATE.name(), timeStamp, 1, Boolean.TRUE);

        when(managementRepository.findOne(1L)).thenReturn(event);

        managementService.deleteEvent(1l);

        assertEquals(event.getPushed(), Boolean.FALSE);
        assertEquals(event.getUpdatedBy(), Integer.valueOf(1));
        assertEquals(event.getEventType(), EventType.DELETE.name());
        assertFalse(event.getTimeStamp() == timeStamp);

        verify(managementRepository, times(1)).save(event);
    }

    @Test
    public void deleteEventNull() throws Exception {
        Event event = null;

        when(managementRepository.findOne(1L)).thenReturn(event);

        managementService.deleteEvent(1l);

        verify(managementRepository, times(0)).save(event);
    }

    @Test
    public void deleteEventError() throws Exception {
        Date timeStamp = new Date();
        Event event = new Event(EventType.UPDATE.name(), timeStamp, 1, Boolean.TRUE);

        when(managementRepository.findOne(1L)).thenReturn(event);
        doThrow(Exception.class).when(managementRepository).save(event);

        managementService.deleteEvent(1l);

        verify(managementRepository, times(1)).save(event);
    }
}